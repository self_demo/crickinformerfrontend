import { Component, OnInit } from '@angular/core';
import { ApiCallService } from '../../services/api-call.service';
import { MatchCardComponent } from '../../components/match-card/match-card.component';

@Component({
  selector: 'app-live',
  standalone: true,
  imports: [MatchCardComponent],
  templateUrl: './live.component.html',
  styleUrl: './live.component.css',
})
export class LiveComponent implements OnInit {
  liveMatches: any;

  constructor(private apiCallService: ApiCallService) {}
  ngOnInit(): void {
    this.loadLiveMetches();
  }

  private loadLiveMetches() {
    this.apiCallService.getLiveMatches().subscribe({
      next: (data) => {
        console.log(data);
        this.liveMatches=data
      },
      error: (data) => {
        console.log(data);
      },
    });
  }
}
